const ExtractTextPlugin = require("extract-text-webpack-plugin")

class CleanUpStatsPlugin {
  shouldPickStatChild(child) {
    return child.name.indexOf('extract-text-webpack-plugin') !== 0
  }

  apply(compiler) {
    compiler.plugin('done', stats => {
      if (Array.isArray(stats.compilation.children)) {
        stats.compilation.children = stats.compilation.children.filter(child => this.shouldPickStatChild(child))
      }
    })
  }
}

module.exports = (bundler, options = {}) => {
  if (options.extract) {
    bundler
      .rules([
        {
          test: /\.s(a|c)ss$/,
          use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: [
              { loader: 'css-loader' },
              {
                loader: 'postcss-loader',
                options: {
                  ident: 'postcss',
                  plugins: loader => [
                    require('autoprefixer')(),
                  ]
                }
              },
              { loader: 'sass-loader' },
            ]
          }),
        }
      ])
      .plugins([
        new ExtractTextPlugin({
          filename: '[name].css.liquid',
        }),
        new CleanUpStatsPlugin()
      ])
  } else {

    bundler
      .rules([
        {
          test: /\.s(a|c)ss$/,
          use: [
            { loader: 'style-loader' },
            { loader: 'css-loader' },
            {
              loader: 'postcss-loader',
              options: {
                ident: 'postcss',
                plugins: loader => [
                  require('autoprefixer')(),
                ]
              }
            },
            { loader: 'sass-loader' },
          ]
        }
      ])
  }



}