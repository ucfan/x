const webpack = require('webpack')

const hotEntry = entry => {
  const client = 'webpack-hot-middleware/client?path=https://localhost:8088/__webpack_hmr&reload=true'

  return Object.keys(entry).reduce((result, key) => {
    if (Array.isArray(entry[key])) {
      result[key] = [client].concat(entry[key])
    } else {
      result[key] = [
        client,
        entry[key],
      ]
    }
    return result
  }, {})
}

module.exports = (bundler, options = {}) => {
  bundler
    .plugins([
      new webpack.HotModuleReplacementPlugin(options),
    ])
    .extend(config => {
      config.output.publicPath = 'https://localhost:8088/'
      config.entry = hotEntry(config.entry)
    })
}
