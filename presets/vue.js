module.exports = (bundler, options) => {
  bundler
    .rules([
      {
        test: /\.js$/,
        use: {
          loader: 'babel-loader',
          options: options.babel || {},
        },
        exclude: /(node_modules|bower_components)/
      },

      {
        // vue single file component
        test: /\.vue$/,
        loader: 'vue-loader',
      },
    ])
    .provide({
      Vue: ['vue', 'default'],
    })
    .extend(config => {
      config.resolve.alias['vue$'] = 'vue/dist/vue.esm.js',
      config.resolve.extensions.push('.vue')
    })
}