const https = require('https')
const fs = require('fs')
const webpack = require('webpack')
const path = require('path')
const express = require('express')
const cors = require('cors')
const app = express()

const sslConfig = {
  key: fs.readFileSync(path.join(__dirname, './ssl/localhost.key')),
  cert: fs.readFileSync(path.join(__dirname, './ssl/localhost.cert')),
}

function createServer(compiler) {
  const done = {}

  compiler.plugin('done', (stats) => {
    const process = ({ compilation: { assets } }) => {
      Object.keys(assets).forEach(key => {
        if (key.match(/inject-js.liquid$/) && !done[key]) {
          const asset = assets[key]
          done[key] = true
          fs.writeFileSync(asset.existsAt, asset.source())
        }
      })
    }

    if (stats.hasOwnProperty('stats') && Array.isArray(stats.stats)) {
      stats.stats.forEach(process)
    } else {
      process(stats)
    }
  })

  app.use(cors())

  app.use(require('webpack-dev-middleware')(compiler, {}))

  app.use(require('webpack-hot-middleware')(compiler, {
    log: false,
    heartbeat: 2000,
  }))

  return https.createServer(sslConfig, app)
}

module.exports = (config) => {
  const compiler = webpack(config)
  const server = createServer(compiler)
  server.listen(8088)
}