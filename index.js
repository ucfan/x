const webpack = require('webpack')
const Bundler = require('./bundler.js')

class X {
  constructor() {
    this.debug = process.env.NODE_ENV !== 'production'
  }

  static use(plugin) {
    plugin(X)
  }

  bundle(options) {
    return new Bundler(this, options)
  }

  run(config) {
    const compiler = webpack(config, this._compilerCallback)
    compiler.run(this._compilerCallback)
  }

  watch(config) {
    const compiler = webpack(config, this._compilerCallback)
    compiler.watch({}, this._compilerCallback)
  }

  serve(config) {
    require('./server')(config)
  }

  _compilerCallback(err, stats) {
    if (err) {
      console.error(err.stack || err)
      if (err.details) {
        console.error(err.details)
      }
      process.exitCode = 1
      return
    }

    const output = stats.toString({
      chunks: false,
      colors: true,
    })
    console.log(output)
  }
}

// X.use(require('./plugins/sass'))

module.exports = new X()
module.exports.webpack = webpack