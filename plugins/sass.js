function sass(options, presetOptions) {
  return this
    .bundle(options)
    .preset('sass', presetOptions)
}

function install(X) {
  X.prototype.sass = sass
}

module.exports = install