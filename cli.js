#!/usr/bin/env node

const path = require('path')
const x = require('./index.js')
const program = require('commander')

function resolveConfig() {
  if (!program.config) {
    console.log('Config file is missing.')
    process.exit(1)
    return
  }

  const configPath = path.resolve(process.cwd(), program.config)
  return require(configPath)(x)
}

program
  .version('1.0.0')
  .option('-c, --config [config]', 'path to config file')

program
  .command('build').alias('b')
  .action(() => {
    x.run(resolveConfig())
  })

program
  .command('serve').alias('s')
  .action(() => {
    const config = resolveConfig()
    // console.log(config)
    x.serve(config)
  })

program.parse(process.argv)

if (!program.args.length) {
  program.help()
}