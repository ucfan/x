const path = require('path')
const fs = require('fs')
const webpack = require('webpack')

const HtmlWebpackPlugin = require('html-webpack-plugin')

class Bundler {
  constructor(context, options) {
    this.context = context
    this._config = this._defaultConfig(options)
    this.builders = []
  }

  get config() {
    return this.builders
      .reduce((config, callback) => {
        callback(config)
        return config
      }, Object.assign({}, this._config))
  }

  _defaultConfig(options = {}) {
    return {
      entry: {},
      output: {
        path: options.path || process.cwd(),
        filename: options.filename || '[name].js',
      },
      plugins: [
        new webpack.DefinePlugin({
          'process.env': {
            NODE_ENV: this.context.debug ? '"development"' : '"production"'
          }
        }),
      ],
      module: {
        rules: []
      },
      resolve: {
        alias: {},
        extensions: ['.js', '.json'],
        modules: ['bower_components', 'node_modules'],
        descriptionFiles: ['package.json', 'bower.json'],
      }
    }
  }

  _resolveEntries(dirname) {
    const files = fs.readdirSync(dirname)
    return files.reduce((entries, file) => {
      const filepath = path.join(dirname, file)

      const stat = fs.statSync(filepath)
      if (stat.isDirectory()) {
        return entries
      }

      const ext = path.extname(file)
      const key = path.basename(file, ext)
      entries[key] = filepath
      return entries
    }, {})
  }

  dest(pathname) {
    this._config.output.path = pathname
    return this
  }

  entry(options) {
    if (!this._config.entry) {
      this._config.entry = {}
    }

    Object.assign(this._config.entry, options)
    return this
  }

  entryDir(dirname) {
    const entries = this._resolveEntries(dirname)
    return this.entry(entries)
  }

  preset(presetName, options) {
    const presetFn = require(`./presets/${presetName}`)
    presetFn(this, options)
    return this
  }

  extend(callback) {
    this.builders.push(callback)
    return this
  }

  vendor(options) {
    const _config = this._config

    if (!_config.entry.vendor) {
      _config.entry.vendor = []
      this.common({ name: 'vendor', filename: 'vendor.js' })
    }

    _config.entry.vendor = _config.entry.vendor.concat(options.filter(option => option))
    return this
  }

  common(options) {
    const defaultOptions = {
      name: 'commons',
      filename: 'commons.js'
    }

    return this.plugins([
      new webpack.optimize.CommonsChunkPlugin(options || defaultOptions)
    ])
  }

  uglify(options) {
    return this.plugins([
      new webpack.optimize.UglifyJsPlugin(options),
    ])
  }

  provide(options) {
    const _config = this._config
    const plugin = _config.plugins.find(plugin => plugin.constructor.name === 'ProvidePlugin')

    if (plugin) {
      Object.assign(plugin.definitions, options)
      return this
    }

    return this.plugins([
      new webpack.ProvidePlugin(options),
    ])
  }

  alias(options) {
    const _config = this._config

    if (!_config.resolve) {
      _config.resolve = {}
    }
    if (!_config.resolve.alias) {
      _config.resolve.alias = {}
    }

    Object.assign(_config.resolve.alias, options)
    return this
  }

  html(options) {
    return this.plugins([
      new HtmlWebpackPlugin({
        filename: options.filename,
        template: options.template,
        inject: false,
      }),
    ])
  }

  plugins(options) {
    const _config = this._config

    if (!_config.plugins) {
      _config.plugins = []
    }

    _config.plugins = _config.plugins.concat(options.filter(option => option))
    return this
  }

  rules(options) {
    const _config = this._config

    if (!_config.module.rules) {
      _config.module.rules = []
    }

    _config.module.rules = _config.module.rules.concat(options.filter(option => option))
    return this
  }
}

module.exports = Bundler